import arrow from "../../img/icons/Arrow 1.png";

export class NewsHTML {
  #items = [];
  #rootNode = document.querySelector(".news-cards");

  constructor(data = []) {
    data.forEach((el) => this.#createItem(el));
  }

  #createItem(item) {
    this.#items.push(`
      <li class="news-cards-item">
        <img src="${item.poster}" alt="news icon" />
        <div class="news-cards-item-content">
          <p>
            ${item.title}
          </p>
          <a href="/news/${item._id}">Детальніше <img src="${arrow}" alt="news arrow" /></a>
        </div>
      </li>
    `);
  }

  render() {
    this.#rootNode.innerHTML = this.#items.join("\n");
  }
}
