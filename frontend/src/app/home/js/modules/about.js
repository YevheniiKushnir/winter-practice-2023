export class AboutHTML {
  #rootContacts = document.querySelector(".contacts");
  #rootAbout = document.querySelector(".about");
  #rootHeader = document.querySelector(".header");

  constructor(about, contacts) {
    this.about = about;
    this.contacts = contacts;
  }

  #injectContactsData() {
    this.#rootContacts.querySelector("#phone").innerHTML = this.contacts.phone;
    this.#rootContacts.querySelector("#location").innerHTML = this.contacts.locations;
    this.#rootContacts.querySelector("#email").innerHTML = this.contacts.email;
    this.#rootContacts.querySelector(".contacts-body-social_media").innerHTML =
      this.contacts.social_media
        .map(
          (el) => `
            <li>
              <a href="${el.link}">
                <img src="${el.image}" alt="mail icon" />
              </a>
            </li>
          `
        )
        .join("\n");
  }

  #injectAboutData() {
    this.#rootAbout.querySelector(".about-history").innerHTML = this.about.history;
    this.#rootAbout.querySelector(".about-mission").innerHTML = this.about.mission;
    this.#rootAbout.querySelector(".about-value").innerHTML = this.about.values;
    this.#rootAbout.querySelector(".about-collaboration").innerHTML = this.about.partners.join(" ");

    this.#rootAbout.querySelector(".about-title").innerHTML = this.about.information
      .split(". ")
      .join("<br/><br/>");
  }

  #injectHeaderData() {
    const [title, ...data] = this.about.title.split(". ");
    this.#rootHeader.querySelector(".header-information-title").innerHTML = title;
    this.#rootHeader.querySelector(".header-information-text").innerHTML = data.join(". ");
  }

  render() {
    this.#injectContactsData();
    this.#injectAboutData();
    this.#injectHeaderData();
  }
}
