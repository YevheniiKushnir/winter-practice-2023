export class ServicesHTML {
  #items = [];
  #rootNode = document.querySelector(".services-list");

  constructor(data = []) {
    data.forEach((el) => this.#createItem(el));
  }

  #createItem(item) {
    const steps = item.steps
      .map(
        (el) => `
          <li class="services-list-item-steps-item">
            ${el}
          </li>
        `
      )
      .join("\n");

    this.#items.push(`
      <article class="services-list-item">
        <div class="services-list-item-body">
          <div class="services-list-item-body-content">
            <h4 class="services-list-item-body-content-title">${item.title}</h4>
            <p class="services-list-item-body-content-short_description">
              ${item.short_description}
            </p>
            <p class="services-list-item-body-content-description">
              ${item.description}
            </p>
          </div>
          <img src="${item.poster}" alt="article image" />
        </div>
        <ol class="services-list-item-steps">
          ${steps}
        </ol>
      </article>
    `);
  }

  render() {
    this.#rootNode.innerHTML = this.#items.join("\n");
  }
}
