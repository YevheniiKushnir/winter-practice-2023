export class VacancyHTML {
  #items = [];
  #rootNode = document.querySelector(".vacancy-cards");

  constructor(data = []) {
    data.forEach((el) => this.#createItem(el));
  }

  #createItem(item) {
    const required_criteria = item.required_criteria
      .map(
        (el) => `
      <li>${el}</li>
    `
      )
      .join("\n");

    this.#items.push(`
      <li class="vacancy-cards-item">
        <h5 class="vacancy-cards-item-title">${item.title}</h5>
        <p class="vacancy-cards-item-description">
          ${item.description}
        </p>
        <div class="vacancy-cards-item-criterias">
          <p>Вимоги до кандидата:</p>
          <ul>
            ${required_criteria}
          </ul>
          <p>Якщо Ви вважаєш, що підходите нам, чекаємо на ваше резюме!</p>
        </div>
      </li>
    `);
  }

  render() {
    this.#rootNode.innerHTML = this.#items.join("\n");
  }
}
