import "../styles/style.scss";

import { aboutApi, appApi, newsApi, servicesApi, vacancyApi } from "../../../shared/js/appApi";

import { ServicesHTML } from "./modules/services";
import { NewsHTML } from "./modules/news";
import { VacancyHTML } from "./modules/vacancies";
import { AboutHTML } from "./modules/about";

class AppClient {
  #supportForm = document.querySelector(".support-form");
  #vacancyOrder = document.querySelector(".vacancy-order-form");
  #rootLayout = document.querySelector(".layout");

  async #handleSupport(event) {
    try {
      event.preventDefault();

      await appApi.postSupport({
        comment: this.#supportForm.comment.value,
        phone: this.#supportForm.phone.value,
        email: this.#supportForm.email.value,
      });

      this.#supportForm.reset();
    } catch (error) {
      console.warn(error);
    }
  }

  async #handlerVacancy(event) {
    try {
      event.preventDefault();

      await vacancyApi.order({
        bio: this.#vacancyOrder.bio.value,
        phone: this.#vacancyOrder.phone.value,
        email: this.#vacancyOrder.email.value,
        vacancy_id: this.#vacancyOrder.vacancy_id.value,
        comment: this.#vacancyOrder.comment.value,
      });

      this.#vacancyOrder.reset();
    } catch (error) {
      console.warn(error);
    }
  }

  async #getData() {
    const [{ services }, { news }, { vacancies }, { about }, { contacts }] = await Promise.all([
      servicesApi.getAll,
      newsApi.getAll,
      vacancyApi.getAll,
      aboutApi.getAll,
      aboutApi.getContacts,
    ]);

    new ServicesHTML(services).render();
    new NewsHTML(news).render();
    new VacancyHTML(vacancies).render();
    new AboutHTML(about, contacts).render();

    this.#rootLayout.classList.add("hidden");
  }

  active() {
    this.#getData();
    this.#supportForm.addEventListener("submit", this.#handleSupport.bind(this));
    this.#vacancyOrder.addEventListener("submit", this.#handlerVacancy.bind(this));
  }
}

const appClient = new AppClient();
appClient.active();
