export const servicesApi = {
  getAll: fetch("/api/services").then((r) => r.json()),
};

export const newsApi = {
  getAll: fetch("/api/news").then((r) => r.json()),
};

export const vacancyApi = {
  getAll: fetch("/api/vacancies").then((r) => r.json()),
  order: (data) =>
    fetch("/api/vacancies/order", {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify(data),
    }),
};

export const appApi = {
  postSupport: (data) =>
    fetch("/api/about/support_service", {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify(data),
    }),
};

export const aboutApi = {
  getAll: fetch("/api/about").then((r) => r.json()),
  getContacts: fetch("/api/about/contacts").then((r) => r.json()),
};
