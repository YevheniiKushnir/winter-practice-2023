## Work strategy

- For start writing code, run command in root dir terminal

        npm run fe:start

- For getting list of backend api, run project and open new tab

        http://localhost:8080/api-docs

### Files structure

For each web page you have to create new folder in src/app/[dir]

#### Created dir has to have next structure

    page dir
        - js
    	    - main.js - root
    	    - other js files
        - styles
    	    - styles.scss - root
    	    - other scss files
        - img - all images for page
        - index.html

### Shared dir

In this dir should be store repeated code

    shared dir
        - js
    	    - other js files
        - styles
    	    - other scss files
        - other folders

### NPM dependencies

If you want to use some new npm package, you can add it. But add it into your dir.
