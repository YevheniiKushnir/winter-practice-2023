const path = require("path");
const glob = require("glob");

const miniCss = require("mini-css-extract-plugin");
const HtmlWebpackPlugin = require("html-webpack-plugin");

const OUTPUT_PATH = path.resolve(__dirname, "../backend/client");
const PATH_TO_JS = "./src/app/**/js/main.js";
const PATH_TO_HTML = "./src/app/**/index.html";
const DIR_NAME_REGEX = /([a-zA-Z0-9]{1,})\/js/i;
const DIR_NAME_HTML_REGEX = /.+\/([a-zA-Z0-9]{1,})\/[a-zA-Z]{1,}.html/i;

function generateEntries() {
  const entry = {};

  glob.sync(PATH_TO_JS).forEach((file) => {
    const name = file.match(DIR_NAME_REGEX)[1];
    entry[name] = file;
  });

  return entry;
}
function generateHtmlTemplatePlugin() {
  return glob
    .sync(PATH_TO_HTML)
    .map((file) => {
      return {
        name: file.match(DIR_NAME_HTML_REGEX)[1],
        fileName: file.match(DIR_NAME_HTML_REGEX)[1],
        path: file,
      };
    })
    .map(
      (template) =>
        new HtmlWebpackPlugin({
          template: template.path,
          inject: "body",
          chunks: [template.name.toString()],
          filename: `pages/${template.name}/index.html`,
        })
    );
}

module.exports = (env) => {
  const isDev = env.isDev === "dev" ? true : false;
  return {
    mode: isDev ? "development" : "production",
    entry: generateEntries(),
    output: {
      filename: isDev ? "static/js/main.[name].js" : "static/js/main.[name].[hash:6].bundle.js",
      path: OUTPUT_PATH,
      assetModuleFilename: "static/[hash][ext][query]",
      clean: true,
    },
    devtool: isDev ? "source-map" : false,
    plugins: [
      new miniCss({
        filename: () =>
          isDev ? "static/styles/style.[name].css" : "static/styles/style.[name].[hash:6].min.css",
      }),
      ...generateHtmlTemplatePlugin(),
    ],
    module: {
      rules: [
        {
          test: /\.s[ac]ss$/i,
          use: [
            miniCss.loader,
            {
              loader: "css-loader",
              options: {
                sourceMap: isDev,
              },
            },
            {
              loader: "sass-loader",
              options: {
                sourceMap: isDev,
              },
            },
          ],
        },
        {
          test: /\.html$/i,
          use: "html-loader",
        },
        {
          test: /\.(png|svg|jpg|jpeg|gif)$/i,
          type: "asset/resource",
        },
      ],
    },
  };
};
