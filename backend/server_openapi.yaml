openapi: 3.0.0
info:
  title: 'Backend REST API'
  version: 1.0.0
  contact:
    name: 'Kushnir E.'
servers:
  - url: /
schemes:
  - 'http'
  - 'https'

paths:
  /:
    get:
      tags:
        - Pages
      summary: Get home page
  /api/news:
    get:
      tags:
        - News
      summary: Get all news
      responses:
        '200':
          description: Success
          content:
            application/json:
              schema:
                type: object
                properties:
                  news:
                    type: array
                    items:
                      $ref: '#/components/schemas/News'
        '500':
          description: Error
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Error'
    post:
      tags:
        - News
      summary: Create new news
      security:
        - jwt_token: []
      requestBody:
        required: true
        content:
          multipart/form-data:
            schema:
              type: object
              properties:
                title:
                  type: string
                description:
                  type: string
                poster:
                  type: string
                  format: binary
      responses:
        '200':
          description: Success
          content:
            application/json:
              schema:
                type: object
                properties:
                  message:
                    type: string
                  news_id:
                    type: string
        '400':
          description: Warning
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Warning'
        '500':
          description: Error
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Error'
  /api/news/{id}:
    get:
      tags:
        - News
      summary: Get news by id
      parameters:
        - in: 'path'
          name: 'id'
          description: 'News id'
          required: true
          type: string
      responses:
        '200':
          description: Success
          content:
            application/json:
              schema:
                type: object
                properties:
                  news:
                    $ref: '#/components/schemas/News'
        '500':
          description: Error
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Error'
    put:
      tags:
        - News
      summary: Update news by id
      parameters:
        - in: 'path'
          name: 'id'
          description: 'News id'
          required: true
          type: string
      security:
        - jwt_token: []
      requestBody:
        required: true
        content:
          multipart/form-data:
            schema:
              type: object
              properties:
                title:
                  type: string
                description:
                  type: string
                poster:
                  type: string
                  format: binary
      responses:
        '200':
          description: Success
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Success'
        '400':
          description: Warning
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Warning'
        '500':
          description: Error
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Error'
    delete:
      tags:
        - News
      summary: Delete news by id
      security:
        - jwt_token: []
      parameters:
        - in: 'path'
          name: 'id'
          description: 'News id'
          required: true
          type: string
      responses:
        '200':
          description: Success
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Success'
        '500':
          description: Error
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Error'
  /api/auth/login:
    post:
      tags:
        - Auth
      summary: Login
      requestBody:
        required: true
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/User'

      responses:
        '200':
          description: Success
          content:
            application/json:
              schema:
                type: object
                properties:
                  jwt_token:
                    type: string
                    example: 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJ0b3B0YWwuY29tIiwiZXhwIjoxNDI2NDIwODAwLCJodHRwOi8vdG9wdGFsLmNvbS9qd3RfY2xhaW1zL2lzX2FkbWluIjp0cnVlLCJjb21wYW55IjoiVG9wdGFsIiwiYXdlc29tZSI6dHJ1ZX0.yRQYnWzskCZUxPwaQupWkiUzKELZ49eM7oWxAQK_ZXw'
        '400':
          description: Warning
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Warning'
        '500':
          description: Error
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Error'
  /api/auth/register:
    post:
      tags:
        - Auth
      summary: Register a new system user(Manager or Administrator)
      security:
        - jwt_token: []
      requestBody:
        required: true
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/Credentials'

      responses:
        '200':
          description: Success
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Success'
        '400':
          description: Warning
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Warning'
        '500':
          description: Error
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Error'
  /api/about:
    get:
      tags:
        - About
      summary: Get all information about company
      responses:
        '200':
          description: Success
          content:
            application/json:
              schema:
                type: object
                properties:
                  about:
                    $ref: '#/components/schemas/About'
        '500':
          description: Error
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Error'
    put:
      tags:
        - About
      summary: Update information about company
      security:
        - jwt_token: []
      requestBody:
        required: true
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/About'
      responses:
        '200':
          description: Success
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Success'
        '400':
          description: Warning
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Warning'
        '500':
          description: Error
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Error'
  /api/about/contacts:
    get:
      tags:
        - About
      summary: Get company contacts
      responses:
        '200':
          description: Success
          content:
            application/json:
              schema:
                type: object
                properties:
                  contacts:
                    $ref: '#/components/schemas/Contacts'
        '500':
          description: Error
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Error'
    put:
      tags:
        - About
      summary: Update company contacts
      description: Exemple of code for extract data (https://codesandbox.io/s/naughty-buck-7g54b1?file=/src/index.js)
      security:
        - jwt_token: []
      requestBody:
        required: true
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/ContactsRequest'
      responses:
        '200':
          description: Success
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Success'
        '400':
          description: Warning
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Warning'
        '500':
          description: Error
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Error'
    patch:
      tags:
        - About
      summary: Add new company contacts social media
      security:
        - jwt_token: []
      requestBody:
        required: true
        content:
          multipart/form-data:
            schema:
              $ref: '#/components/schemas/ContactsSocialMediaRequest'
      responses:
        '200':
          description: Success
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Success'
        '400':
          description: Warning
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Warning'
        '500':
          description: Error
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Error'
    delete:
      tags:
        - About
      summary: Delete company contacts social media
      security:
        - jwt_token: []
      requestBody:
        required: true
        content:
          application/json:
            schema:
              type: object
              properties:
                name:
                  type: string
                  example: 'social media link name'
              required:
                - name
      responses:
        '200':
          description: Success
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Success'
        '500':
          description: Error
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Error'
  /api/about/support_service:
    get:
      tags:
        - About
      summary: Get all requests to support service
      security:
        - jwt_token: []
      responses:
        '200':
          description: Success
          content:
            application/json:
              schema:
                type: object
                properties:
                  requests:
                    type: array
                    items:
                      allOf:
                        - $ref: '#/components/schemas/SupportService'
                        - type: 'object'
                          properties:
                            request_id:
                              type: string
        '500':
          description: Error
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Error'
    post:
      tags:
        - About
      summary: Create new request to support service
      requestBody:
        required: true
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/SupportService'
      responses:
        '200':
          description: Success
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Success'
        '400':
          description: Warning
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Warning'
        '500':
          description: Error
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Error'
  /api/about/support_service/{id}:
    delete:
      tags:
        - About
      summary: Delete request to support service
      security:
        - jwt_token: []
      parameters:
        - in: 'path'
          name: 'id'
          description: 'Request to support service id'
          required: true
          type: string
      responses:
        '200':
          description: Success
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Success'
        '500':
          description: Error
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Error'
  /api/services:
    get:
      tags:
        - Services
      summary: Get all services
      responses:
        '200':
          description: Success
          content:
            application/json:
              schema:
                type: object
                properties:
                  services:
                    type: array
                    items:
                      allOf:
                        - $ref: '#/components/schemas/Services'
                        - type: 'object'
                          properties:
                            _id:
                              type: string
                            steps:
                              type: array
                              items:
                                type: string
                            poster:
                              type: string
                              example: '/upload/services/dsdsd.png'
                            createdAt:
                              type: string

        '500':
          description: Error
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Error'
    post:
      tags:
        - Services
      summary: Create new service
      description: Warning! for field "steps" use next rules 1-use only one item, 2-wrap data with "["(at start) and "]"(at the end) like [{text:ssfff},{text:sdfs}]
      security:
        - jwt_token: []
      requestBody:
        required: true
        content:
          multipart/form-data:
            schema:
              $ref: '#/components/schemas/Services'
      responses:
        '200':
          description: Success
          content:
            application/json:
              schema:
                allOf:
                  - $ref: '#/components/schemas/Success'
                  - type: 'object'
                    properties:
                      service_id:
                        type: string
        '400':
          description: Warning
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Warning'
        '500':
          description: Error
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Error'
  /api/services/{id}:
    get:
      tags:
        - Services
      summary: Get service by id
      parameters:
        - in: 'path'
          name: 'id'
          description: 'Service id'
          required: true
          type: string
      responses:
        '200':
          description: Success
          content:
            application/json:
              schema:
                type: object
                properties:
                  service:
                    allOf:
                      - $ref: '#/components/schemas/Services'
                      - type: 'object'
                        properties:
                          _id:
                            type: string
                          steps:
                            type: array
                            items:
                              type: string
                          poster:
                            type: string
                            example: '/upload/services/dsdsd.png'
                          createdAt:
                            type: string

        '500':
          description: Error
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Error'
    put:
      tags:
        - Services
      summary: Update service by id
      description: Warning! for field "steps" use next rules 1-use only one item, 2-wrap data with "["(at start) and "]"(at the end) like [{text:ssfff},{text:sdfs}]
      parameters:
        - in: 'path'
          name: 'id'
          description: 'Service id'
          required: true
          type: string
      security:
        - jwt_token: []
      requestBody:
        required: true
        content:
          multipart/form-data:
            schema:
              $ref: '#/components/schemas/Services'
      responses:
        '200':
          description: Success
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Success'
        '400':
          description: Warning
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Warning'
        '500':
          description: Error
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Error'
    delete:
      tags:
        - Services
      summary: Delete service by id
      security:
        - jwt_token: []
      parameters:
        - in: 'path'
          name: 'id'
          description: 'Service id'
          required: true
          type: string
      responses:
        '200':
          description: Success
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Success'
        '500':
          description: Error
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Error'
  /api/services/order:
    get:
      tags:
        - Services
      summary: Get all orders
      security:
        - jwt_token: []
      responses:
        '200':
          description: Success
          content:
            application/json:
              schema:
                type: object
                properties:
                  orders:
                    type: array
                    items:
                      allOf:
                        - $ref: '#/components/schemas/ServicesOrderRequest'
                        - type: 'object'
                          properties:
                            _id:
                              type: string
                            service_id:
                              type: string
                            createdAt:
                              type: string
        '500':
          description: Error
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Error'
    post:
      tags:
        - Services
      summary: Create new order service
      requestBody:
        required: true
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/ServicesOrderRequest'
      responses:
        '200':
          description: Success
          content:
            application/json:
              schema:
                allOf:
                  - $ref: '#/components/schemas/Success'
                  - type: 'object'
                    properties:
                      order_id:
                        type: string
        '400':
          description: Warning
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Warning'
        '500':
          description: Error
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Error'
  /api/services/order/{id}:
    delete:
      tags:
        - Services
      summary: Delete order by id
      security:
        - jwt_token: []
      parameters:
        - in: 'path'
          name: 'id'
          description: 'Order id'
          required: true
          type: string
      responses:
        '200':
          description: Success
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Success'
        '500':
          description: Error
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Error'
  /api/vacancies:
    get:
      tags:
        - Vacancies
      summary: Get all vacancies
      responses:
        '200':
          description: Success
          content:
            application/json:
              schema:
                type: object
                properties:
                  vacancies:
                    type: array
                    items:
                      allOf:
                        - $ref: '#/components/schemas/Vacancies'
                        - type: 'object'
                          properties:
                            _id:
                              type: string
                            createdAt:
                              type: string

        '500':
          description: Error
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Error'
    post:
      tags:
        - Vacancies
      summary: Create new vacancy
      security:
        - jwt_token: []
      requestBody:
        required: true
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/Vacancies'
      responses:
        '200':
          description: Success
          content:
            application/json:
              schema:
                allOf:
                  - $ref: '#/components/schemas/Success'
                  - type: 'object'
                    properties:
                      vacancy_id:
                        type: string
        '400':
          description: Warning
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Warning'
        '500':
          description: Error
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Error'
  /api/vacancies/{id}:
    delete:
      tags:
        - Vacancies
      summary: Delete vacancy by id
      security:
        - jwt_token: []
      parameters:
        - in: 'path'
          name: 'id'
          description: 'vacancy id'
          required: true
          type: string
      responses:
        '200':
          description: Success
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Success'
        '500':
          description: Error
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Error'
    get:
      tags:
        - Vacancies
      summary: Get vacancy by id
      parameters:
        - in: 'path'
          name: 'id'
          description: 'vacancy id'
          required: true
          type: string
      responses:
        '200':
          description: Success
          content:
            application/json:
              schema:
                type: object
                properties:
                  vacancy:
                    allOf:
                      - $ref: '#/components/schemas/Vacancies'
                      - type: 'object'
                        properties:
                          _id:
                            type: string
                          createdAt:
                            type: string
        '500':
          description: Error
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Error'
    put:
      tags:
        - Vacancies
      summary: Update vacancy by id
      parameters:
        - in: 'path'
          name: 'id'
          description: 'vacancy id'
          required: true
          type: string
      security:
        - jwt_token: []
      requestBody:
        required: true
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/Vacancies'
      responses:
        '200':
          description: Success
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Success'
        '400':
          description: Warning
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Warning'
        '500':
          description: Error
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Error'
  /api/vacancies/order:
    get:
      tags:
        - Vacancies
      summary: Get all vacancy orders
      security:
        - jwt_token: []
      responses:
        '200':
          description: Success
          content:
            application/json:
              schema:
                type: object
                properties:
                  orders:
                    type: array
                    items:
                      allOf:
                        - $ref: '#/components/schemas/VacanciesOrderRequest'
                        - type: 'object'
                          properties:
                            _id:
                              type: string
                            createdAt:
                              type: string
        '500':
          description: Error
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Error'
    post:
      tags:
        - Vacancies
      summary: Create new vacancy order
      requestBody:
        required: true
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/VacanciesOrderRequest'
      responses:
        '200':
          description: Success
          content:
            application/json:
              schema:
                allOf:
                  - $ref: '#/components/schemas/Success'
                  - type: 'object'
                    properties:
                      order_id:
                        type: string
        '400':
          description: Warning
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Warning'
        '500':
          description: Error
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Error'
  /api/vacancies/order/{id}:
    delete:
      tags:
        - Vacancies
      summary: Delete vacancy order by id
      security:
        - jwt_token: []
      parameters:
        - in: 'path'
          name: 'id'
          description: 'Vacancy id'
          required: true
          type: string
      responses:
        '200':
          description: Success
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Success'
        '500':
          description: Error
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Error'

components:
  securitySchemes:
    jwt_token:
      type: 'apiKey'
      name: 'Authorization'
      in: 'header'

  schemas:
    Error:
      type: 'object'
      properties:
        message:
          type: 'string'
          description: 'Error message'
          example: 'Error message'
    Success:
      type: 'object'
      properties:
        message:
          type: 'string'
          description: 'Success message'
          example: 'Success message'
    Warning:
      type: 'object'
      properties:
        message:
          type: 'string'
          description: 'Warning message'
          example: 'Warning message'
    Credentials:
      allOf:
        - $ref: '#/components/schemas/User'
        - type: 'object'
          properties:
            name:
              type: 'string'
              example: 'test name'
            role:
              type: 'string'
              enum: [MANAGER, ADMINISTRATOR]
              example: 'ADMINISTRATOR'
      required:
        - role
    User:
      type: 'object'
      properties:
        email:
          type: 'string'
          example: 'Kyrylo@g.com'
        password:
          type: 'string'
          example: 've518dl3'
      required:
        - email
        - password
    News:
      type: 'object'
      properties:
        _id:
          type: 'string'
          description: 'id of acticle'
        title:
          type: 'string'
          description: 'new service'
        description:
          type: 'string'
          description: 'description of article'
        poster:
          type: 'string'
          description: 'path to photo'
        createdAt:
          type: 'string'
          format: 'date'
          description: 'date of creation'
      example:
        _id: '2232442'
        title: 'the new artilce'
        description: 'the description article'
        createdAt: '2023-02-10T04:05:06.157Z'
        poster: '/upload/news/23232.jpg'
    About:
      type: 'object'
      properties:
        title:
          type: string
          example: 'main title'
        information:
          type: string
          example: 'information text'
        slogan:
          type: string
          example: 'slogan text'
        history:
          type: string
          example: 'history text'
        mission:
          type: string
          example: 'mission text'
        values:
          type: string
          example: 'values text'
        partners:
          type: array
          items:
            type: string
            example: 'partners text'
        awards_certificates:
          type: array
          items:
            type: string
            example: 'Awards and certificates text'
    Contacts:
      type: 'object'
      properties:
        locations:
          type: string
          example: 'test locations'
        phone:
          type: string
          example: '3809876523'
        email:
          type: string
          example: 'company@g.com'
        social_media:
          type: array
          items:
            type: object
            properties:
              name:
                type: string
                example: 'Instagram'
              link:
                type: string
                example: 'https:/inst.com'
              image:
                type: string
                example: '/upload/contacts/24244.png'
    ContactsRequest:
      type: object
      properties:
        locations:
          type: string
          example: 'test locations'
        phone:
          type: string
          example: '3809876523'
        email:
          type: string
          example: 'company@g.com'
    ContactsSocialMediaRequest:
      type: object
      properties:
        name:
          type: string
          example: 'test name'
        link:
          type: string
          example: 'test link'
        file:
          type: file
    Services:
      type: object
      properties:
        title:
          type: string
        short_description:
          type: string
        description:
          type: string
        steps:
          type: array
          items:
            type: object
            properties:
              text:
                type: string
        poster:
          type: string
          format: binary
    ServicesOrderRequest:
      type: object
      properties:
        bio:
          type: string
        phone:
          type: string
        email:
          type: string
        service_id:
          type: string
        company:
          type: string
        comment:
          type: string
      required:
        - bio
        - phone
        - email
        - service_id
    Vacancies:
      type: object
      properties:
        title:
          type: string
        description:
          type: string
        required_criteria:
          type: array
          items:
            type: string
    VacanciesOrderRequest:
      type: object
      properties:
        bio:
          type: string
        phone:
          type: string
        email:
          type: string
        vacancy_id:
          type: string
        comment:
          type: string
      required:
        - bio
        - phone
        - email
        - vacancy_id
    SupportService:
      type: object
      properties:
        email:
          type: string
        phone:
          type: string
        comment:
          type: string
      required:
        - phone
        - email
