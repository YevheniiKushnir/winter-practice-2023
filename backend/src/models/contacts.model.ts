import Joi from 'joi';
import mongoose from 'mongoose';
import {
  IContactsRequest,
  IContactsResponse,
  IContactsSocialMedia,
} from '../types';

export const schemaJoiContacts = Joi.object<IContactsRequest>({
  locations: Joi.string().required(),
  phone: Joi.string().required(),
  email: Joi.string()
    .email({ minDomainSegments: 2, tlds: { allow: ['com', 'net'] } })
    .required(),
}).error(new Error('Unvalid contacts data'));

export const schemaJoiContactsSocialMedia = Joi.object<IContactsSocialMedia>({
  name: Joi.string().required(),
  link: Joi.string().required(),
}).error(new Error('Unvalid contacts social media data'));

const contactsSchema = new mongoose.Schema<IContactsResponse>(
  {
    locations: {
      type: String,
      required: true,
    },
    phone: {
      type: String,
      required: true,
    },
    email: {
      type: String,
      required: true,
    },
    social_media: {
      type: [
        {
          name: String,
          link_name: String,
          link: String,
          relativePath: String,
          buffer: Buffer,
        },
      ],
      required: true,
    },
  },
  { timestamps: true }
);

const ContactsModel = mongoose.model('Contacts', contactsSchema);

async function createOneDoc() {
  try {
    const countDoc = await ContactsModel.countDocuments();
    if (countDoc === 0) {
      const doc = new ContactsModel({
        locations: 'test locations',
        phone: 'test phone',
        email: 'test@tote.com',
        social_media: [],
      });
      await doc.save();
    }
  } catch {
    console.warn('Error when created Contacts doc');
  }
}

createOneDoc();

export default ContactsModel;
