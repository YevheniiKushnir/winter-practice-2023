import Joi from 'joi';
import mongoose from 'mongoose';
import { IAbout } from '../types';

export const schemaJoiAbout = Joi.object<IAbout>({
  title: Joi.string().required(),
  information: Joi.string().required(),
  slogan: Joi.string().required(),
  history: Joi.string().required(),
  mission: Joi.string().required(),
  values: Joi.string().required(),
  partners: Joi.array().required(),
  awards_certificates: Joi.array().required(),
}).error(new Error('Unvalid about data'));

const aboutSchema = new mongoose.Schema<IAbout>(
  {
    title: {
      type: String,
      required: true,
    },
    information: {
      type: String,
      required: true,
    },
    slogan: {
      type: String,
      required: true,
    },
    history: {
      type: String,
      required: true,
    },
    mission: {
      type: String,
      required: true,
    },
    values: {
      type: String,
      required: true,
    },
    partners: {
      type: Array(String),
      required: true,
    },
    awards_certificates: {
      type: Array(String),
      required: true,
    },
  },
  { timestamps: true }
);

const AboutModel = mongoose.model('About', aboutSchema);

async function createOneDoc() {
  try {
    const countDoc = await AboutModel.countDocuments();
    if (countDoc === 0) {
      const doc = new AboutModel({
        title: 'test title',
        information: 'test information',
        slogan: 'test slogan',
        history: 'test history',
        mission: 'test mission',
        values: 'test values',
        partners: ['test partners'],
        awards_certificates: ['test awards_certificates'],
      });
      await doc.save();
    }
  } catch {
    console.warn('Error when created About doc');
  }
}

createOneDoc();

export default AboutModel;
