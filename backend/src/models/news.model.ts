import Joi from 'joi';
import mongoose from 'mongoose';
import { INews, INewsCreateRequest } from '../types';

export const schemaJoiNews = Joi.object<Omit<INewsCreateRequest, 'file'>>({
  title: Joi.string().required(),
  description: Joi.string().required(),
}).error(new Error('Unvalid news data'));

const newsSchema = new mongoose.Schema<INews>(
  {
    title: {
      type: String,
      required: true,
    },
    description: {
      type: String,
      required: true,
    },
    poster: {
      type: {
        name: String,
        relativePath: String,
        buffer: Buffer,
      },
      required: true,
    },
  },
  { timestamps: true }
);

const NewsModel = mongoose.model('News', newsSchema);

export default NewsModel;
