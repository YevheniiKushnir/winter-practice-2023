import Joi from 'joi';
import mongoose from 'mongoose';
import bcrypt from 'bcryptjs';

import { IUser } from '../types';
import { ROLE_ADMIN, ROLE_MANAGER } from '../app_constants';

export const schemaJoiPersonRegister = Joi.object<IUser>({
  email: Joi.string()
    .email({ minDomainSegments: 2, tlds: { allow: ['com', 'net'] } })
    .required()
    .error(new Error('Email must be valid')),
  password: Joi.string()
    .pattern(/^[a-zA-Z0-9]{3,40}$/i)
    .required()
    .error(new Error('Password must be valid')),
  role: Joi.string()
    .valid(ROLE_ADMIN, ROLE_MANAGER)
    .required()
    .error(new Error('Role must be valid')),
  name: Joi.string().error(new Error('Name must be valid')),
});

export const schemaJoiPersonLogin = Joi.object<IUser>({
  email: Joi.string()
    .email({ minDomainSegments: 2, tlds: { allow: ['com', 'net'] } })
    .required()
    .error(new Error('Email must be valid')),
  password: Joi.string()
    .pattern(/^[a-zA-Z0-9]{3,40}$/i)
    .required()
    .error(new Error('Password must be valid')),
});

const personSchema = new mongoose.Schema<IUser>(
  {
    email: {
      type: String,
      required: true,
      unique: true,
    },
    password: {
      type: String,
      required: true,
    },
    role: {
      type: String,
      enum: [ROLE_ADMIN, ROLE_MANAGER],
      required: true,
    },
    name: {
      type: String,
    },
  },
  { timestamps: true }
);

const PersonModel = mongoose.model('Users', personSchema);

async function createTwoDoc() {
  try {
    const countDoc = await PersonModel.countDocuments();
    if (countDoc === 0) {
      const admin = new PersonModel({
        name: 'admin',
        role: ROLE_ADMIN,
        email: process.env.ADMIN_EMAIL as string,
        password: bcrypt.hashSync(
          process.env.ADMIN_PASSWORD as string,
          bcrypt.genSaltSync(7)
        ),
      });
      const manager = new PersonModel({
        name: 'manager',
        role: ROLE_MANAGER,
        email: process.env.MANAGER_EMAIL as string,
        password: bcrypt.hashSync(
          process.env.MANAGER_PASSWORD as string,
          bcrypt.genSaltSync(7)
        ),
      });

      await admin.save();
      await manager.save();
    }
  } catch {
    console.warn('Error when created root Persons docs');
  }
}

createTwoDoc();

export default PersonModel;
