import Joi from 'joi';
import mongoose from 'mongoose';
import { IServiceOrder } from '../types';

export const schemaJoiServiceOrder = Joi.object<IServiceOrder>({
  bio: Joi.string().required(),
  email: Joi.string()
    .email({ minDomainSegments: 2, tlds: { allow: ['com', 'net'] } })
    .required()
    .error(new Error('Email must be valid')),
  phone: Joi.string().required(),
  service_id: Joi.string().required(),
  comment: Joi.string(),
  company: Joi.string(),
}).error(new Error('Unvalid service order data'));

const ServiceOrderSchema = new mongoose.Schema<IServiceOrder>(
  {
    bio: {
      type: String,
      required: true,
    },
    email: {
      type: String,
      required: true,
    },
    phone: {
      type: String,
      required: true,
    },
    service_id: {
      type: String,
      required: true,
    },
    comment: {
      type: String,
    },
    company: {
      type: String,
    },
  },
  { timestamps: true }
);

const ServiceOrderModel = mongoose.model('Service_Orders', ServiceOrderSchema);

export default ServiceOrderModel;
