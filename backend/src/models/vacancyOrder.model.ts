import Joi from 'joi';
import mongoose from 'mongoose';
import { IVacancyOrder } from '../types';

export const schemaJoiVacancyOrder = Joi.object<IVacancyOrder>({
  bio: Joi.string().required(),
  email: Joi.string()
    .email({ minDomainSegments: 2, tlds: { allow: ['com', 'net'] } })
    .required()
    .error(new Error('Email must be valid')),
  phone: Joi.string().required(),
  vacancy_id: Joi.string().required(),
  comment: Joi.string(),
}).error(new Error('Unvalid vacancy order data'));

const vacancyOrderSchema = new mongoose.Schema<IVacancyOrder>(
  {
    bio: {
      type: String,
      required: true,
    },
    email: {
      type: String,
      required: true,
    },
    phone: {
      type: String,
      required: true,
    },
    vacancy_id: {
      type: String,
      required: true,
    },
    comment: {
      type: String,
    },
  },
  { timestamps: true }
);

const VacancyOrderModel = mongoose.model('Vacancy_Orders', vacancyOrderSchema);

export default VacancyOrderModel;
