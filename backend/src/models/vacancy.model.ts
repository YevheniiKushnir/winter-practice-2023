import Joi from 'joi';
import mongoose from 'mongoose';
import { IVacancy } from '../types';

export const schemaJoiVacancy = Joi.object<IVacancy>({
  title: Joi.string().required(),
  description: Joi.string().required(),
  required_criteria: Joi.array().required(),
}).error(new Error('Unvalid vacancy data'));

const vacancySchema = new mongoose.Schema<IVacancy>(
  {
    title: {
      type: String,
      required: true,
    },
    description: {
      type: String,
      required: true,
    },
    required_criteria: {
      type: Array(String),
      required: true,
    },
  },
  { timestamps: true }
);

const VacancyModel = mongoose.model('Vacancies', vacancySchema);

export default VacancyModel;
