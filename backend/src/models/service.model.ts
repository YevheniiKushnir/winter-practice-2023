import Joi from 'joi';
import mongoose from 'mongoose';
import { IService, IServiceCreateRequest } from '../types';

export const schemaJoiService = Joi.object<
  Omit<IServiceCreateRequest, 'poster'>
>({
  title: Joi.string().required(),
  description: Joi.string().required(),
  short_description: Joi.string().required(),
  steps: Joi.array().required(),
}).error(new Error('Unvalid service data'));

const serviceSchema = new mongoose.Schema<IService>(
  {
    title: {
      type: String,
      required: true,
    },
    description: {
      type: String,
      required: true,
    },
    short_description: {
      type: String,
      required: true,
    },
    poster: {
      type: {
        name: String,
        relativePath: String,
        buffer: Buffer,
      },
      required: true,
    },
    steps: {
      type: [
        {
          text: String,
        },
      ],
      required: true,
    },
  },
  { timestamps: true }
);

const ServiceModel = mongoose.model('Services', serviceSchema);

export default ServiceModel;
