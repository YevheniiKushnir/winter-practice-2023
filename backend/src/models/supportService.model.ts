import Joi from 'joi';
import mongoose from 'mongoose';
import { ISupportService } from '../types';

export const schemaJoiSupportService = Joi.object<ISupportService>({
  email: Joi.string()
    .email({ minDomainSegments: 2, tlds: { allow: ['com', 'net'] } })
    .required()
    .error(new Error('Email must be valid')),
  phone: Joi.string().required(),
  comment: Joi.string(),
}).error(new Error('Unvalid support service data'));

const supportServiceSchema = new mongoose.Schema<ISupportService>(
  {
    email: {
      type: String,
      required: true,
    },
    phone: {
      type: String,
      required: true,
    },
    comment: {
      type: String,
    },
  },
  { timestamps: true }
);

const SupportServiceModel = mongoose.model(
  'Support_Service',
  supportServiceSchema
);

export default SupportServiceModel;
