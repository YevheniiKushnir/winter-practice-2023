import { Request, Response, NextFunction } from 'express';
import * as jwt from 'jsonwebtoken';
import { IUser } from '../types';

declare global {
  // eslint-disable-next-line @typescript-eslint/no-namespace
  namespace Express {
    interface Request {
      user: IUser;
    }
  }
}

function secureRoutes(req: Request, res: Response, next: NextFunction) {
  const { authorization } = req.headers;

  if (!authorization) {
    return res
      .status(401)
      .json({ message: 'Please, provide authorization header' });
  }

  const token: string | undefined = authorization;

  if (!token) {
    return res
      .status(401)
      .json({ message: 'Please, include token to request' });
  }

  try {
    const jwtData = jwt.verify(
      token,
      process.env.JWT_SECRET_KEY as jwt.Secret
    ) as IUser;

    req.user = { ...jwtData };

    next();
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
  } catch (err: any) {
    return res.status(401).json({ message: err.message });
  }
}

export default secureRoutes;
