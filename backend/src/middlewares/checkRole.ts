import { Request, Response, NextFunction } from 'express';
import { IUser } from '../types';

declare global {
  // eslint-disable-next-line @typescript-eslint/no-namespace
  namespace Express {
    interface Request {
      user: IUser;
    }
  }
}

function chechRole(role: string) {
  return (req: Request, res: Response, next: NextFunction) => {
    if (role !== req.user.role)
      return res.status(400).json({ message: 'Missed role' });
    next();
  };
}

export default chechRole;
