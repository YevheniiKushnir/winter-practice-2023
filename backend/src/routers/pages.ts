import { join } from 'path';
import { Router } from 'express';
import { pathToClientPagesDir } from '../app_paths';

const router = Router();

router.get('/', (req, res) => {
  res.sendFile(join(pathToClientPagesDir, 'home/index.html'));
});

export default router;
