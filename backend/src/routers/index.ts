import { Router } from 'express';
import rootPages from './pages';

const router = Router();

router.use(rootPages);

export default router;
