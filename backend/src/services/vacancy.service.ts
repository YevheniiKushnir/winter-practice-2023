import VacancyOrderData from '../models/vacancyOrder.model';
import VacancyData from '../models/vacancy.model';

import {
  IVacancy,
  IVacancyOrder,
  IVacancyOrderResponse,
  IVacancyResponse,
} from '../types';

class VacancyService {
  constructor(
    private VacancyModel = VacancyData,
    private VacancyOrderModel = VacancyOrderData
  ) {}

  async create(data: IVacancy) {
    const vacancy = new this.VacancyModel(data);

    await vacancy.save();

    return vacancy;
  }

  async createOrder(data: IVacancyOrder) {
    const order = new this.VacancyOrderModel(data);

    await order.save();

    return order;
  }

  async getAll() {
    const data = await this.VacancyModel.find<IVacancyResponse>({}, '-__v');

    return data;
  }

  async getAllOrders() {
    const orders = await this.VacancyOrderModel.find<IVacancyOrderResponse>(
      {},
      '-__v'
    );

    return orders;
  }

  async getById(id: string) {
    const data = await this.VacancyModel.findById<IVacancyResponse>(
      { _id: id },
      '-__v'
    );

    if (!data) throw new Error('Vacancy not founded');

    return data;
  }

  async deleteById(id: string) {
    await this.VacancyModel.deleteOne({ _id: id });
  }

  async deleteOrderById(id: string) {
    await this.VacancyOrderModel.deleteOne({ _id: id });
  }

  async updateById(id: string, data: IVacancy) {
    const vacancy = await this.VacancyModel.findById({ _id: id }, '-__v');

    if (!vacancy) throw new Error('News not founded');

    vacancy.title = data.title;
    vacancy.description = data.description;
    vacancy.required_criteria = data.required_criteria;

    await vacancy.save();

    return vacancy;
  }
}

export default new VacancyService();
