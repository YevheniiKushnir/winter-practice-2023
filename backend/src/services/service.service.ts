import { randomUUID } from 'crypto';
import { join } from 'path';
import { existsSync, mkdirSync } from 'fs';

import { UploadedFile } from 'express-fileupload';

import ServiceData from '../models/service.model';
import ServiceOrderData from '../models/serviceOrder.model';

import isExistFile from '../helpers/isExistFile';
import { pathForUploadService, pathToServiceDir } from '../app_paths';
import {
  IService,
  IServiceOrder,
  IServiceOrderResponse,
  IServiceResponse,
} from '../types';

class ServiceService {
  private extractFileData(poster: UploadedFile) {
    const fileName = `${randomUUID()}-${poster.name}`;
    const filePath = join(pathToServiceDir, fileName);

    if (!existsSync(pathToServiceDir)) {
      mkdirSync(pathToServiceDir, { recursive: true });
    }

    poster.mv(filePath, (err) => {
      if (err) throw Error(err.message);
    });

    return {
      name: fileName,
      buffer: poster.data,
      relativePath: `${pathForUploadService}${fileName}`,
    };
  }

  constructor(
    private ServiceModel = ServiceData,
    private ServiceOrderModel = ServiceOrderData
  ) {}

  async create(data: Omit<IService, 'poster'>, poster: UploadedFile) {
    const fileData = this.extractFileData(poster);

    const service = new this.ServiceModel({
      ...data,
      poster: fileData,
    });

    await service.save();

    return service;
  }

  creteOrder(data: IServiceOrder) {
    return new this.ServiceOrderModel(data);
  }

  async getAll() {
    const data = await this.ServiceModel.find<IServiceResponse>({}, '-__v');

    await isExistFile(data, pathToServiceDir, 'poster');

    return data;
  }

  async getAllOrders() {
    const data = await this.ServiceOrderModel.find<IServiceOrderResponse>(
      {},
      '-__v'
    );

    return data;
  }

  async updateById(
    id: string,
    data: Omit<IService, 'poster'>,
    poster: UploadedFile
  ) {
    const service = await this.ServiceModel.findOne({ _id: id }, '-__v');

    if (!service) throw new Error('Service not founded');

    const fileData = this.extractFileData(poster);

    service.title = data.title;
    service.short_description = data.short_description;
    service.description = data.description;
    service.steps = data.steps;
    service.poster = fileData;

    await service.save();

    return service;
  }

  async deleteById(id: string) {
    await this.ServiceModel.deleteOne({ _id: id });
  }

  async deleteOrderById(id: string) {
    await this.ServiceOrderModel.deleteOne({ _id: id });
  }

  async getById(id: string) {
    const data = await this.ServiceModel.findOne({ _id: id }, '-__v');

    await isExistFile([data], pathToServiceDir, 'poster');

    return data;
  }
}

export default new ServiceService();
