import { Request, Response } from 'express';
import bcrypt from 'bcryptjs';
import jwt from 'jsonwebtoken';

import PersonModel, {
  schemaJoiPersonRegister,
  schemaJoiPersonLogin,
} from '../models/auth.model';

import { IUser } from '../types';

export const login = async (req: Request, res: Response) => {
  try {
    const data: Omit<IUser, 'name' | 'role'> = req.body;

    await schemaJoiPersonLogin.validateAsync(data);

    const user = await PersonModel.findOne<IUser & { _id: string }>({
      email: data.email,
    });

    if (!user) throw Error('User not exist');

    const comparePasswords = bcrypt.compareSync(data.password, user.password);

    if (comparePasswords) {
      // eslint-disable-next-line no-underscore-dangle
      const payload = { email: user.email, userId: user._id, role: user.role };

      const jwtToken = jwt.sign(payload, process.env.JWT_SECRET_KEY as string);

      return res.status(200).json({
        jwt_token: jwtToken,
      });
    }

    throw new Error('Not authorized');
  } catch (error: any) {
    res.status(500).send({
      message: error.message ?? 'Failed operation, try again',
    });
  }
};

export const registration = async (req: Request, res: Response) => {
  try {
    const data: IUser = req.body;

    await schemaJoiPersonRegister.validateAsync(data);

    const user = new PersonModel({
      ...data,
      password: bcrypt.hashSync(data.password, bcrypt.genSaltSync(7)),
    });

    await user.save();

    res.json({
      message: 'New user created',
    });
  } catch (error: any) {
    res.status(500).send({
      message: error.message ?? 'Failed operation, try again',
    });
  }
};
