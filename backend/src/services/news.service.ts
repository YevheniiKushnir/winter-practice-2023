import { join } from 'path';
import { existsSync, mkdirSync } from 'fs';
import { randomUUID } from 'crypto';

import { UploadedFile } from 'express-fileupload';

import NewsData from '../models/news.model';

import isExistFile from '../helpers/isExistFile';
import { INews, INewsResponse } from '../types';
import { pathForUploadNews, pathToNewsDir } from '../app_paths';

class NewsService {
  private extractFileData(poster: UploadedFile) {
    const fileName = `${randomUUID()}-${poster.name}`;
    const filePath = join(pathToNewsDir, fileName);

    if (!existsSync(pathToNewsDir)) {
      mkdirSync(pathToNewsDir, { recursive: true });
    }

    poster.mv(filePath, (err) => {
      if (err) throw Error(err.message);
    });

    return {
      name: fileName,
      buffer: poster.data,
      relativePath: `${pathForUploadNews}${fileName}`,
    };
  }

  constructor(private NewsModel = NewsData) {}

  async create(data: Omit<INews, 'poster'>, poster: UploadedFile) {
    const fileData = this.extractFileData(poster);

    const news = new this.NewsModel({
      title: data.title,
      description: data.description,
      poster: fileData,
    });

    await news.save();

    return news;
  }

  async getAll() {
    const data = await this.NewsModel.find<INewsResponse>({}, '-__v');

    await isExistFile(data, pathToNewsDir, 'poster');

    return data;
  }

  async getById(id: string) {
    const data = await this.NewsModel.findOne<INewsResponse>(
      { _id: id },
      '-__v'
    );

    await isExistFile([data], pathToNewsDir, 'poster');

    return data;
  }

  async updateById(
    id: string,
    data: Omit<INews, 'poster'>,
    poster: UploadedFile
  ) {
    const news = await this.NewsModel.findById({ _id: id }, '-__v');

    if (!news) throw new Error('News not founded');

    const fileData = this.extractFileData(poster);

    news.title = data.title;
    news.description = data.description;
    news.poster = fileData;

    await news.save();

    return news;
  }

  async deleteById(id: string) {
    await this.NewsModel.deleteOne({ _id: id });
  }
}

export default new NewsService();
