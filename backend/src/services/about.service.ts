import { randomUUID } from 'crypto';
import { join } from 'path';
import { existsSync, mkdirSync } from 'fs';
import fileUpload, { UploadedFile } from 'express-fileupload';

import SupportServiceModel from '../models/supportService.model';
import AboutModel from '../models/about.model';
import ContactsModel from '../models/contacts.model';

import isExistFile from '../helpers/isExistFile';

import {
  IAbout,
  IContactsRequest,
  IContactsSocialMedia,
  ISupportService,
  ISupportServiceResponse,
} from '../types';
import { pathForUploadSocialMedia, pathToSocialMediaDir } from '../app_paths';

class AboutService {
  private extractFileData(poster: UploadedFile) {
    const fileName = `${randomUUID()}-${poster.name}`;
    const filePath = join(pathToSocialMediaDir, fileName);

    if (!existsSync(pathToSocialMediaDir)) {
      mkdirSync(pathToSocialMediaDir, { recursive: true });
    }

    poster.mv(filePath, (err) => {
      if (err) throw Error(err.message);
    });

    return {
      name: fileName,
      buffer: poster.data,
      relativePath: `${pathForUploadSocialMedia}${fileName}`,
    };
  }

  constructor(
    private SupportServiceData = SupportServiceModel,
    private AboutData = AboutModel,
    private ContactsData = ContactsModel
  ) {}

  async createSupportService(data: ISupportService) {
    const order = new this.SupportServiceData(data);

    await order.save();

    return order;
  }

  async createContactsSocialMedia(
    data: IContactsSocialMedia,
    file: UploadedFile
  ) {
    const doc = await this.ContactsData.find({}, '-__v');
    const contacts = doc[0];

    const fileData = {
      ...this.extractFileData(file),
      link_name: data.name,
      link: data.link,
    };

    contacts.social_media = [
      ...contacts.social_media,
      {
        ...fileData,
      },
    ];

    await contacts.save();

    return contacts;
  }

  async getAllSupportServices() {
    const data = this.SupportServiceData.find<ISupportServiceResponse>(
      {},
      '-__v'
    );

    return data;
  }

  async getAbout() {
    const doc = await this.AboutData.find({}, '-__v');
    const about = doc[0];

    return about;
  }

  async getContacts() {
    const doc = await this.ContactsData.find({}, '-__v');
    const contacts = doc[0];

    await isExistFile(contacts.social_media, pathToSocialMediaDir);

    return contacts;
  }

  async deleteSupportServiceById(id: string) {
    await this.SupportServiceData.deleteOne({ _id: id });
  }

  async deleteContactsSocialMedia(name: string) {
    const doc = await this.ContactsData.find({}, '-__v');
    const contacts = doc[0];

    contacts.social_media = contacts.social_media.filter(
      (el) => el.link_name.trim().toLowerCase() !== name.trim().toLowerCase()
    );

    await contacts.save();

    return contacts;
  }

  async updateAbout(data: IAbout) {
    const doc = await this.AboutData.find({}, '-__v');
    const about = doc[0];

    about.title = data.title;
    about.history = data.history;
    about.information = data.information;
    about.slogan = data.slogan;
    about.mission = data.mission;
    about.values = data.values;
    about.partners = data.partners;
    about.awards_certificates = data.awards_certificates;

    await about.save();

    return about;
  }

  async updateContacts(data: IContactsRequest) {
    const doc = await this.ContactsData.find({}, '-__v');
    const contacts = doc[0];

    contacts.locations = data.locations;
    contacts.email = data.email;
    contacts.phone = data.phone;

    await contacts.save();

    return contacts;
  }
}

export default new AboutService();
