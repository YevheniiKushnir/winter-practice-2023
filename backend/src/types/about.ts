export type ISupportService = {
  email: string;
  phone: string;
  comment?: string;
};

export type ISupportServiceResponse = ISupportService & {
  _id: string;
  createdAt: string;
};

export type IAbout = {
  title: string;
  information: string;
  slogan: string;
  history: string;
  mission: string;
  values: string;
  partners: string[];
  awards_certificates: string[];
};

export type IContactsResponse = {
  locations: string;
  phone: string;
  email: string;
  social_media: {
    name: string;
    link_name: string;
    link: string;
    relativePath: string;
    buffer: Buffer;
  }[];
};

export type IContactsRequest = {
  locations: string;
  phone: string;
  email: string;
};

export type IContactsSocialMedia = {
  name: string;
  link: string;
};
