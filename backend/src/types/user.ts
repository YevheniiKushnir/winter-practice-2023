import { ROLE_ADMIN, ROLE_MANAGER } from '../app_constants';

export type IUser = {
  email: string;
  password: string;
  role: typeof ROLE_ADMIN | typeof ROLE_MANAGER;
  name?: string;
};
