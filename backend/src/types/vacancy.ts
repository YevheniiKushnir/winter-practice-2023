export type IVacancy = {
  title: string;
  description: string;
  required_criteria: string[];
};

export type IVacancyResponse = IVacancy & { _id: string; createdAt: string };

export type IVacancyOrder = {
  vacancy_id: string;
  bio: string;
  phone: string;
  email: string;
  comment?: string;
};

export type IVacancyOrderResponse = IVacancyOrder & {
  _id: string;
  createdAt: string;
};
