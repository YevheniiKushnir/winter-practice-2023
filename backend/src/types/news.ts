import { UploadedFile } from 'express-fileupload';

export type INews = {
  title: string;
  description: string;
  poster: {
    name: string;
    relativePath: string;
    buffer: Buffer;
  };
};

export type INewsCreateRequest = {
  title: string;
  description: string;
  poster: UploadedFile;
};

export type INewsResponse = INews & {
  _id: string;
  poster: string;
  createdAt?: string;
};
