import { UploadedFile } from 'express-fileupload';

export type IService = {
  title: string;
  short_description: string;
  description: string;
  steps: { text: string }[];
  poster: {
    name: string;
    relativePath: string;
    buffer: Buffer;
  };
};

export type IServiceCreateRequest = {
  title: string;
  short_description: string;
  description: string;
  steps: { text: string }[];
  poster: UploadedFile;
};

export type IServiceResponse = IService & {
  _id: string;
  poster: string;
  createdAt?: string;
};

export type IServiceOrder = {
  bio: string;
  phone: string;
  email: string;
  service_id: string;
  company?: string;
  comment?: string;
};

export type IServiceOrderResponse = IServiceOrder & {
  _id: string;
  createdAt: string;
};
