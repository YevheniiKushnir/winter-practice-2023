import { existsSync, mkdirSync, writeFile } from 'fs';
import { join } from 'path';

function createFile(file: any, pathToDir: string) {
  return new Promise((res, rej) => {
    const pathToFile = join(pathToDir, file.name);

    writeFile(pathToFile, file.buffer, (err) => {
      if (err) {
        return rej(err);
      }
      return res(null);
    });
  });
}

export default async (data: any[], pathToDir: string, fileKey?: string) => {
  if (!existsSync(pathToDir)) {
    mkdirSync(pathToDir, { recursive: true });
  }

  const filtredData = data
    .map((el) =>
      existsSync(join(pathToDir, fileKey ? el[fileKey].name : el.name))
        ? null
        : createFile(fileKey ? el[fileKey] : el, pathToDir)
    )
    .filter(Boolean);

  return Promise.all(filtredData);
};
