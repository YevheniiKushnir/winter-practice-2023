import { Router } from 'express';

import secureRoutes from '../middlewares/secureRoutes';

import vacancyController from './controllers/vacancy.controller';

const router = Router();

router.get('/', vacancyController.getAll);
router.get('/order', secureRoutes, vacancyController.getAllOrders);
router.get('/:id', vacancyController.getById);

router.post('/', secureRoutes, vacancyController.create);
router.post('/order', vacancyController.createOrder);
router.put('/:id', secureRoutes, vacancyController.updateById);

router.delete('/:id', secureRoutes, vacancyController.deleteById);
router.delete('/order/:id', secureRoutes, vacancyController.deleteOrderById);

export default router;
