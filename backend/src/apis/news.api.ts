import { Router } from 'express';

import secureRoutes from '../middlewares/secureRoutes';

import newsController from './controllers/news.controller';

const router = Router();

router.get('/', newsController.getAll);
router.get('/:id', newsController.getById);

router.post('/', secureRoutes, newsController.create);
router.put('/:id', secureRoutes, newsController.updateById);

router.delete('/:id', secureRoutes, newsController.deleteById);

export default router;
