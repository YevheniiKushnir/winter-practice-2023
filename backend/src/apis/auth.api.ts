import { Router } from 'express';
import { ROLE_MANAGER } from '../app_constants';
import chechRole from '../middlewares/checkRole';
import secureRoutes from '../middlewares/secureRoutes';

import { login, registration } from '../services/auth.service';

const router = Router();

router.post('/login', login);
router.post('/register', secureRoutes, chechRole(ROLE_MANAGER), registration);

export default router;
