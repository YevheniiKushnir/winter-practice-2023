import { Router } from 'express';

import secureRoutes from '../middlewares/secureRoutes';

import aboutController from './controllers/about.controller';

const router = Router();

router.get('/', aboutController.getAbout);
router.get(
  '/support_service',
  secureRoutes,
  aboutController.getAllSupportService
);
router.get('/contacts', aboutController.getContacts);

router.post('/support_service', aboutController.createSupportService);
router.put('/', secureRoutes, aboutController.updateAbout);
router.put('/contacts', secureRoutes, aboutController.updateContancts);
router.patch(
  '/contacts',
  secureRoutes,
  aboutController.createContactsSocialMedia
);

router.delete(
  '/support_service/:id',
  secureRoutes,
  aboutController.deleteSupportServiceById
);
router.delete(
  '/contacts',
  secureRoutes,
  aboutController.deleteContactsSocialMedia
);

export default router;
