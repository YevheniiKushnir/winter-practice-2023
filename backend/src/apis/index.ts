import { Router } from 'express';

import newsApi from './news.api';
import authApi from './auth.api';
import vacancyApi from './vacancy.api';
import servicesApi from './service.api';
import aboutApi from './about.api';

const router = Router();

router.use('/news', newsApi);
router.use('/auth', authApi);
router.use('/vacancies', vacancyApi);
router.use('/services', servicesApi);
router.use('/about', aboutApi);

export default router;
