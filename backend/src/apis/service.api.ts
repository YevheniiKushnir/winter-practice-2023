import { Router } from 'express';

import secureRoutes from '../middlewares/secureRoutes';

import serviceController from './controllers/service.controller';

const router = Router();

router.get('/', serviceController.getAll);
router.get('/order', secureRoutes, serviceController.getAllOrders);
router.get('/:id', serviceController.getById);

router.post('/', secureRoutes, serviceController.create);
router.post('/order', serviceController.createOrder);
router.put('/:id', secureRoutes, serviceController.updateById);

router.delete('/:id', secureRoutes, serviceController.deleteById);
router.delete('/order/:id', secureRoutes, serviceController.deleteOrderById);

export default router;
