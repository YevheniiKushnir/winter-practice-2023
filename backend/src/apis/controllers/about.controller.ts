import { Request, Response } from 'express';

import { UploadedFile } from 'express-fileupload';
import aboutService from '../../services/about.service';

import { schemaJoiSupportService } from '../../models/supportService.model';
import { schemaJoiAbout } from '../../models/about.model';
import {
  schemaJoiContacts,
  schemaJoiContactsSocialMedia,
} from '../../models/contacts.model';

import {
  IAbout,
  IContactsRequest,
  IContactsSocialMedia,
  ISupportService,
} from '../../types';

class AboutController {
  async createSupportService(req: Request, res: Response) {
    try {
      const data: ISupportService = req.body;

      await schemaJoiSupportService.validateAsync(data);

      await aboutService.createSupportService(data);

      res.status(200).json({
        message: 'created new support service order',
      });
    } catch (error: any) {
      res.status(500).send({
        message: error.message ?? 'Failed operation, try again',
      });
    }
  }

  async createContactsSocialMedia(req: Request, res: Response) {
    try {
      const data: IContactsSocialMedia = req.body;
      const { file } = req.files as { file: UploadedFile };

      await schemaJoiContactsSocialMedia.validateAsync(data);

      await aboutService.createContactsSocialMedia(data, file);

      res.status(200).json({
        message: 'created new contacts social media',
      });
    } catch (error: any) {
      res.status(500).send({
        message: error.message ?? 'Failed operation, try again',
      });
    }
  }

  async getAllSupportService(req: Request, res: Response) {
    try {
      const requests = await aboutService.getAllSupportServices();

      res.status(200).json({
        requests:
          requests.map((r) => ({
            // eslint-disable-next-line no-underscore-dangle
            _id: r._id,
            email: r.email,
            phone: r.phone,
            comment: r.comment,
            createdAt: r.createdAt,
          })) ?? [],
      });
    } catch (error: any) {
      res.status(500).send({
        message: error.message ?? 'Failed operation, try again',
      });
    }
  }

  async getAbout(req: Request, res: Response) {
    try {
      const about = await aboutService.getAbout();

      res.status(200).json({
        about: {
          title: about.title,
          history: about.history,
          information: about.information,
          slogan: about.slogan,
          mission: about.mission,
          values: about.values,
          partners: about.partners,
          awards_certificates: about.awards_certificates,
        },
      });
    } catch (error: any) {
      res.status(500).send({
        message: error.message ?? 'Failed operation, try again',
      });
    }
  }

  async getContacts(req: Request, res: Response) {
    try {
      const contacts = await aboutService.getContacts();

      res.status(200).json({
        contacts: {
          locations: contacts.locations,
          phone: contacts.phone,
          email: contacts.email,
          social_media:
            contacts.social_media.map((el) => ({
              name: el.link_name,
              link: el.link,
              image: el.relativePath,
            })) ?? [],
        },
      });
    } catch (error: any) {
      res.status(500).send({
        message: error.message ?? 'Failed operation, try again',
      });
    }
  }

  async deleteSupportServiceById(req: Request, res: Response) {
    try {
      const { id } = req.params as { id: string };

      await aboutService.deleteSupportServiceById(id);

      res.json({
        message: `Deleted support service order by id-${id}`,
      });
    } catch (error: any) {
      res.status(500).send({
        message: error.message ?? 'Failed operation, try again',
      });
    }
  }

  async deleteContactsSocialMedia(req: Request, res: Response) {
    try {
      const { name } = req.body as { name: string };

      await aboutService.deleteContactsSocialMedia(name);

      res.json({
        message: `Deleted contacts social media`,
      });
    } catch (error: any) {
      res.status(500).send({
        message: error.message ?? 'Failed operation, try again',
      });
    }
  }

  async updateAbout(req: Request, res: Response) {
    try {
      const data: IAbout = req.body;

      await schemaJoiAbout.validateAsync(data);

      await aboutService.updateAbout(data);

      res.status(200).json({
        message: `updated about`,
      });
    } catch (error: any) {
      res.status(500).send({
        message: error.message ?? 'Failed operation, try again',
      });
    }
  }

  async updateContancts(req: Request, res: Response) {
    try {
      const data: IContactsRequest = req.body;

      await schemaJoiContacts.validateAsync(data);

      await aboutService.updateContacts(data);

      res.status(200).json({
        message: `updated contacts`,
      });
    } catch (error: any) {
      res.status(500).send({
        message: error.message ?? 'Failed operation, try again',
      });
    }
  }
}

export default new AboutController();
