import { Request, Response } from 'express';
import { UploadedFile } from 'express-fileupload';

import newsService from '../../services/news.service';

import { schemaJoiNews } from '../../models/news.model';
import { INews } from '../../types';

class NewController {
  async create(req: Request, res: Response) {
    try {
      const data: Omit<INews, 'poster'> = req.body;
      const { poster } = req.files as { poster: UploadedFile };

      await schemaJoiNews.validateAsync(data);

      const news = await newsService.create(data, poster);

      res.status(200).json({
        message: 'created new news',
        news_id: news.id,
      });
    } catch (error: any) {
      res.status(500).send({
        message: error.message ?? 'Failed operation, try again',
      });
    }
  }

  async getAll(req: Request, res: Response) {
    try {
      const news = await newsService.getAll();

      res.status(200).json({
        news:
          news.map((n) => ({
            // eslint-disable-next-line no-underscore-dangle
            _id: n._id,
            createdAt: n.createdAt,
            title: n.title,
            description: n.description,
            poster: n.poster.relativePath,
          })) ?? [],
      });
    } catch (error: any) {
      res.status(500).send({
        message: error.message ?? 'Failed operation, try again',
      });
    }
  }

  async getById(req: Request, res: Response) {
    try {
      const { id } = req.params as { id: string };

      const news = await newsService.getById(id);

      if (!news) throw new Error('News not founded');

      res.status(200).json({
        news: {
          // eslint-disable-next-line no-underscore-dangle
          _id: news._id,
          title: news.title,
          description: news.description,
          poster: news.poster.relativePath,
          createdAt: news.createdAt,
        },
      });
    } catch (error: any) {
      res.status(500).send({
        message: error.message ?? 'Failed operation, try again',
      });
    }
  }

  async updateById(req: Request, res: Response) {
    try {
      const { id } = req.params as { id: string };
      const data: Omit<INews, 'poster'> = req.body;
      const { poster } = req.files as { poster: UploadedFile };

      await schemaJoiNews.validateAsync(data);

      const news = await newsService.updateById(id, data, poster);

      res.status(200).json({
        message: `updated news by id-${id}`,
        news_id: news.id,
      });
    } catch (error: any) {
      res.status(500).send({
        message: error.message ?? 'Failed operation, try again',
      });
    }
  }

  async deleteById(req: Request, res: Response) {
    try {
      const { id } = req.params as { id: string };

      await newsService.deleteById(id);

      res.json({
        message: `Deleted news by id-${id}`,
      });
    } catch (error: any) {
      res.status(500).send({
        message: error.message ?? 'Failed operation, try again',
      });
    }
  }
}

export default new NewController();
