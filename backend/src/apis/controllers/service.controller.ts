import { Request, Response } from 'express';
import { UploadedFile } from 'express-fileupload';

import serviceService from '../../services/service.service';

import { schemaJoiService } from '../../models/service.model';
import { schemaJoiServiceOrder } from '../../models/serviceOrder.model';

import { IService, IServiceOrder, IServiceResponse } from '../../types';

class ServiceController {
  async create(req: Request, res: Response) {
    try {
      const data: Omit<IService, 'poster'> = req.body;
      data.steps = JSON.parse(data.steps as unknown as string);
      const { poster } = req.files as { poster: UploadedFile };

      await schemaJoiService.validateAsync(data);

      const service = await serviceService.create(data, poster);

      res.status(200).json({
        message: 'created new service',
        service_id: service.id,
      });
    } catch (error: any) {
      res.status(500).send({
        message: error.message ?? 'Failed operation, try again',
      });
    }
  }

  async createOrder(req: Request, res: Response) {
    try {
      const data: IServiceOrder = req.body;

      await schemaJoiServiceOrder.validateAsync(data);

      const order = serviceService.creteOrder(data);

      await order.save();

      res.status(200).json({
        message: 'created new service',
        order_id: order.id,
      });
    } catch (error: any) {
      res.status(500).send({
        message: error.message ?? 'Failed operation, try again',
      });
    }
  }

  async getAll(req: Request, res: Response) {
    try {
      const services = await serviceService.getAll();

      res.status(200).json({
        services:
          services.map((el) => ({
            // eslint-disable-next-line no-underscore-dangle
            _id: el._id,
            title: el.title,
            short_description: el.short_description,
            description: el.description,
            steps: el.steps.map((t) => t.text),
            poster: el.poster.relativePath,
            createdAt: el.createdAt,
          })) ?? [],
      });
    } catch (error: any) {
      res.status(500).send({
        message: error.message ?? 'Failed operation, try again',
      });
    }
  }

  async getAllOrders(req: Request, res: Response) {
    try {
      const orders = await serviceService.getAllOrders();

      res.status(200).json({
        orders:
          orders.map((el) => ({
            // eslint-disable-next-line no-underscore-dangle
            _id: el._id,
            service_id: el.service_id,
            bio: el.bio,
            email: el.email,
            phone: el.phone,
            comment: el.comment,
            company: el.company,
            createdAt: el.createdAt,
          })) ?? [],
      });
    } catch (error: any) {
      res.status(500).send({
        message: error.message ?? 'Failed operation, try again',
      });
    }
  }

  async updateById(req: Request, res: Response) {
    try {
      const { id } = req.params as { id: string };
      const data: Omit<IService, 'poster'> = req.body;
      data.steps = JSON.parse(data.steps as unknown as string);
      const { poster } = req.files as { poster: UploadedFile };

      await schemaJoiService.validateAsync(data);

      const service = await serviceService.updateById(id, data, poster);

      res.status(200).json({
        message: `updated service by id-${id}`,
        service_id: service.id,
      });
    } catch (error: any) {
      res.status(500).send({
        message: error.message ?? 'Failed operation, try again',
      });
    }
  }

  async deleteById(req: Request, res: Response) {
    try {
      const { id } = req.params as { id: string };

      await serviceService.deleteById(id);

      res.json({
        message: `Deleted service by id-${id}`,
      });
    } catch (error: any) {
      res.status(500).send({
        message: error.message ?? 'Failed operation, try again',
      });
    }
  }

  async deleteOrderById(req: Request, res: Response) {
    try {
      const { id } = req.params as { id: string };

      await serviceService.deleteOrderById(id);

      res.json({
        message: `Deleted service order by id-${id}`,
      });
    } catch (error: any) {
      res.status(500).send({
        message: error.message ?? 'Failed operation, try again',
      });
    }
  }

  async getById(req: Request, res: Response) {
    try {
      const { id } = req.params as { id: string };

      const service = (await serviceService.getById(
        id
      )) as IServiceResponse | null;

      if (!service) throw new Error('Service not founded');

      res.status(200).json({
        service: {
          // eslint-disable-next-line no-underscore-dangle
          _id: service._id,
          title: service.title,
          short_description: service.short_description,
          description: service.description,
          poster: service.poster.relativePath,
          steps: service.steps.map((t) => t.text),
          createdAt: service.createdAt,
        },
      });
    } catch (error: any) {
      res.status(500).send({
        message: error.message ?? 'Failed operation, try again',
      });
    }
  }
}

export default new ServiceController();
