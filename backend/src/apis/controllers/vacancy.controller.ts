import { Request, Response } from 'express';

import vacancyService from '../../services/vacancy.service';

import { schemaJoiVacancy } from '../../models/vacancy.model';
import { schemaJoiVacancyOrder } from '../../models/vacancyOrder.model';

import { IVacancy, IVacancyOrder } from '../../types';

class VacancyController {
  async create(req: Request, res: Response) {
    try {
      const data: IVacancy = req.body;

      await schemaJoiVacancy.validateAsync(data);

      const vacancy = await vacancyService.create(data);

      res.status(200).json({
        message: 'created new vacancy',
        vacancy_id: vacancy.id,
      });
    } catch (error: any) {
      res.status(500).send({
        message: error.message ?? 'Failed operation, try again',
      });
    }
  }

  async createOrder(req: Request, res: Response) {
    try {
      const data: IVacancyOrder = req.body;

      await schemaJoiVacancyOrder.validateAsync(data);

      const order = await vacancyService.createOrder(data);

      res.status(200).json({
        message: 'created new vacancy order',
        order_id: order.id,
      });
    } catch (error: any) {
      res.status(500).send({
        message: error.message ?? 'Failed operation, try again',
      });
    }
  }

  async deleteById(req: Request, res: Response) {
    try {
      const { id } = req.params as { id: string };

      await vacancyService.deleteById(id);

      res.json({
        message: `Deleted vacancy by id-${id}`,
      });
    } catch (error: any) {
      res.status(500).send({
        message: error.message ?? 'Failed operation, try again',
      });
    }
  }

  async deleteOrderById(req: Request, res: Response) {
    try {
      const { id } = req.params as { id: string };

      await vacancyService.deleteOrderById(id);

      res.json({
        message: `Deleted vacancy order by id-${id}`,
      });
    } catch (error: any) {
      res.status(500).send({
        message: error.message ?? 'Failed operation, try again',
      });
    }
  }

  async getAll(req: Request, res: Response) {
    try {
      const vacancies = await vacancyService.getAll();

      res.status(200).json({
        vacancies:
          vacancies.map((el) => ({
            // eslint-disable-next-line no-underscore-dangle
            _id: el._id,
            title: el.title,
            description: el.description,
            required_criteria: el.required_criteria,
            createdAt: el.createdAt,
          })) ?? [],
      });
    } catch (error: any) {
      res.status(500).send({
        message: error.message ?? 'Failed operation, try again',
      });
    }
  }

  async getAllOrders(req: Request, res: Response) {
    try {
      const orders = await vacancyService.getAllOrders();

      res.status(200).json({
        orders:
          orders.map((el) => ({
            // eslint-disable-next-line no-underscore-dangle
            _id: el._id,
            bio: el.bio,
            email: el.email,
            phone: el.phone,
            vacancy_id: el.vacancy_id,
            comment: el.comment,
            createdAt: el.createdAt,
          })) ?? [],
      });
    } catch (error: any) {
      res.status(500).send({
        message: error.message ?? 'Failed operation, try again',
      });
    }
  }

  async getById(req: Request, res: Response) {
    try {
      const { id } = req.params as { id: string };

      const vacancy = await vacancyService.getById(id);

      res.status(200).json({
        vacancy: {
          _id: id,
          title: vacancy.title,
          description: vacancy.description,
          required_criteria: vacancy.required_criteria,
          createdAt: vacancy.createdAt,
        },
      });
    } catch (error: any) {
      res.status(500).send({
        message: error.message ?? 'Failed operation, try again',
      });
    }
  }

  async updateById(req: Request, res: Response) {
    try {
      const { id } = req.params as { id: string };
      const data: IVacancy = req.body;

      await schemaJoiVacancy.validateAsync(data);

      await vacancyService.updateById(id, data);

      res.status(200).json({
        message: `updated vacancy by id-${id}`,
      });
    } catch (error: any) {
      res.status(500).send({
        message: error.message ?? 'Failed operation, try again',
      });
    }
  }
}

export default new VacancyController();
