/* eslint-disable import/first */

import * as dotenv from 'dotenv';

dotenv.config();

import { join } from 'path';
import express, { Errback, Request, Response, NextFunction } from 'express';
import morgan from 'morgan';
import swaggerUi from 'swagger-ui-express';
import mongoose from 'mongoose';
import fileUpload from 'express-fileupload';
import YAML from 'yamljs';

import rootRouter from './routers';
import rootApiRouter from './apis';

const PORT = process.env.PORT ?? 8080;

const swaggerDocument = YAML.load(join(__dirname, '../server_openapi.yaml'));

const app = express();

app.use(express.static(join(__dirname, '../client')));
app.use(express.json());
app.use(morgan('tiny'));
app.use(
  fileUpload({
    limits: { fileSize: 15 * 1024 * 1024 },
  })
);
app.use(rootRouter);
app.use('/api', rootApiRouter);
app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerDocument));

const start = async () => {
  try {
    await mongoose.connect(process.env.MONGO_CONNECT as string);
    app.listen(PORT, () => {
      console.log(`Server have been started on ${PORT} port`);
    });
  } catch (err: any) {
    console.error(`Error on server startup: ${err.message}`);
  }
};

start();

// ERROR HANDLER
app.use((err: Errback, req: Request, res: Response, next: NextFunction) => {
  console.error('err: ', err);
  res.status(500).send({ message: 'Server error' });
});
