import { join, resolve } from 'path';

export const pathToClientPagesDir = resolve(__dirname, '../client/pages');

export const pathToUploadDir = resolve(__dirname, '../client/uploads');

export const pathToNewsDir = join(pathToUploadDir, 'news');
export const pathForUploadNews = '/uploads/news/';

export const pathToServiceDir = join(pathToUploadDir, 'services');
export const pathForUploadService = '/uploads/services/';

export const pathToSocialMediaDir = join(pathToUploadDir, 'social_media');
export const pathForUploadSocialMedia = '/uploads/social_media/';
